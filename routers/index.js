const express = require("express");
const openRouter = express.Router();
const authRouter = express.Router();

const Test = require("../controllers/testController");

openRouter.get("/", Test.sayHello); // Entry test point;

module.exports.OpenRouter = openRouter;
module.exports.AuthRouter = authRouter;
