# Personal Node Template (w/ORM)

This is a personal node tempalte project. Which is help me to fast create the project that with familar file structure. It helps to created the app more faster.

## Requirement

- Node
- Npm
- Mysql

## Technical

- Javascript
- SQL
- Sequelize (ORM)
- Express
- JWT

## How to use

- Set up your database
- Set up your database connetction oin config.js
- Run migration
