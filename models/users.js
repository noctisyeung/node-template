"use strict";

module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define(
    "Users",
    {
      userID: {
        type: DataTypes.BIGINT,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      username: { type: DataTypes.STRING, allowNull: false },
      password: { type: DataTypes.TEXT, allowNull: false },
      nickname: { type: DataTypes.STRING, allowNull: true },
      role: { type: DataTypes.TINYINT, allowNull: false, defaultValue: 9 },
      createdTime: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP")
      },
      updatedTime: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal(
          "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"
        )
      }
    },
    {
      timestamps: false
    }
  );
  Users.associate = function(models) {
    // associations can be defined here
  };
  return Users;
};
