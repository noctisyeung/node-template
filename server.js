const express = require("express");
const app = express();

const { OpenRouter } = require("./routers");

// Global Variable
const port = process.env.PORT || 7788;

app.use("/api", OpenRouter);

app.use((req, res) => {
  res.status(404).json({ status: "error", msg: "Not Found Api" });
});

app.listen(port, () => {
  console.log(`Listening on PORT ${port}`);
});
